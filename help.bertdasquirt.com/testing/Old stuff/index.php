<html>
	<head>
		<title> New Design </title>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	
	<body>
		<div class="container">
			<!-- start of page -->
			<div class="header">
				<h1> Craig Childs<img id="avatar" src="sitephoto.png"/></h1>

			</div>
			
			<div class="content">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			
			<div class="project">
				<img class="project-img-left" src="sitephoto.png"/>
				<span class="project-content-right"> <h2>Lorem ipsum</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo.</p></span>
			</div>
			
			<div class="project">
				<img class="project-img-right" src="sitephoto.png"/>
				<span class="project-content-left"> <h2>Lorem ipsum</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo.</p></span>
			</div>
			
			<div class="footer">
				<a href="https://twitter.com/share" class="twitter-share-button" data-text="I've just visited Bertdasquirts new website! http://bertdasquirt.com" data-via="bertdasquirt94" data-related="bertdasquirt.com">Tweet</a>
				
				<script id="twitter-wjs" src="//platform.twitter.com/widgets.js"></script>
				<iframe src="http://www.facebook.com/plugins/like.php?href=http://bertdasquirt.com"
				        scrolling="no" frameborder="0"
				        style="border:none; width:450px; height:30px"></iframe>
				<p> Copyright &#169; Craig Childs </p>
				
			</div>
			<!-- end of page -->
		</div>
	</body>
</html>