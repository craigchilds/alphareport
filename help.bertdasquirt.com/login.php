<!doctype HTML>

<html>
	<head>
		<link rel="stylesheet" href="style.css" type="text/css">
		<title>Alpha Reporter - Index</title>
		<link href='http://fonts.googleapis.com/css?family=Muli&v2' rel='stylesheet' type='text/css'>
	</head>
	
	<script type="text/javascript" language="javascript">
		function goHome(){
			window.location = "index.html";
		}
					
	</script>
	
	<body>
		<div class="container">
			<div id="top-bar">
				<p id="top-text"> <strong> Current User:</strong> <a href="#" id="username">Admin</a> - Last logged in 10/11/2011 at 12:37pm | <a id="logout" href="#">Logout</a></p>
				
			</div>
			<div id="header">
				<img id="logo" src="/img/logo.png" onclick="goHome()"></img>
				<ul>
					<li><a id="link" href="index.html"> Tickets </a></li>
					<li><a id="link" href="index.html"> Logs </a></li>
					<li><a id="link" href="index.html"> Statistics </a></li>
					<li><a id="link" href="index.html"> Profile </a></li>
					<li id="selected"><a id="link" href="index.html"> Users </a></li>
					<li><a id="link" href="index.html"> Home </a></li>
				</ul>
			</div>
			
			<div id="login">
				<form method="post" action="" id="form">
					<label for="username"> Username: </label><input type="textbox" name="username" placeholder="Username"><br>
					
					<label for="password"> Password: </label><input type="password" name="password" placeholder="Password"><br>
					<input type="submit" name="submit" id="button" value="Login">
				</form>
			</div>
			
			<div id="footer">
				<p> Craig Childs &copy; Alpha Reporter 2011. </p>
				<p>
					<a href="about.html">About Us</a>
					|
					<a href="help.html">Help</a>
					|
					<a href="contact.html">Contact Us</a>
				</p>
			</div>
		</div>
	</body>
</html>