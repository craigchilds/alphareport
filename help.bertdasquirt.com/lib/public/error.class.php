<?php

/* ERROR CLASS
 * LOG
 * DISPLAY
 */
 
 class Error{
 	
 	//PUBLIC PROPERTIES
 	public $message = 0;
 	
 	protected function message($message = false){
 		
 		//An array of error messages and there codes. 
 		$codes = array(101 => "No file was found.",
 			 		   102 => "The file type you are trying to upload is not allowed.",
 			 		   null => "No error has been set for this problem.",
 			 		   104 => "This page does not exist.",
 					   105 => "Your username/password is incorrect. Please try again.",
 		 			   106 => "You have not entered valid credentials.", 
 		 			   107 => "You do not have the access level to view this page.",
					   108 => "The file you are trying to edit is not a text file.",
					   109 => "The values aren't suitable and/or are empty",
					   110 => "That username already exists in the database. Please choose another.",
					   111 => "We were unable to insert the data into the database.",
					   112 => "That user does not exist, and so we cannot gather any information about them.", 
					   113 => "We were unable to get the users from the database.",
					   114 => "The user you requested cannot be found.",
					   115 => "The user does not have any information stored about them. This could be because they haven't added any information or there is a problem with our database.");
 		//get the message for the code.
 		$data = $codes[empty($message) ? null : $message];
 		
 		if(!empty($data)){
 		//echo the data if its not empty.	   
	 		echo '<script type="text/javascript">
	 		  window.onload = function() { document.getElementById(\'error\').onclick = function() { this.style.display = \'none\'; } }            
	 		</script>
	 		<div id="error">
	 			<h4>
	 				Error
	 			</h4>
	 			<p>
	 				'. $data .'
	 			</p>
	 			<em> (Click to close) </em>
	 		</div>';
	 	}
 	}
 	
 }