<?php

//Set out constants
define('SALT', 'whateveryouwant');

class Log{
	
	//PRIVATE PROPERTIES
	private $_filename;
	private $_data;
	private $_destination;
	private $_message;	
	
	//PUBLIC PROPERTIES
	
	/**
	 * @desc Write - what we pass into the log file.
	 * @param Array - An array of what we would like to write to the file.
	 * @param  Destination - The file which you would like to write to. 
	 * @param  State - What do you want to do, r = read, w = write, r+w = read and write 
	 * @how to use - $object_name = new Log(array('1 item', '2 items'), 'log.txt', 'w');
	 * @output - | 1 item | 2 items |
	 */
	function __construct($array, $file, $state){
		$c = ($this->_check($file) ? $this->_setFileName($file) : '');
		$arr_c = (!empty($array) && in_array($state, array('w', 'r+w')) ? true : false);
		
		if($arr_c){
			foreach($array as $item){
				$this->_data .=" | " . $item . " | ";
			}
		}
		
		switch($state){
			case 'r':
				$this->read($this->_filename);
			break;
			case 'w':
				$this->_write();
			break;
			case 'r+w':
				$this->_write();
				$this->read($this->_filename);
			break;
		}
		
	}
	
	/**
	 * @desc Write method - Writing to a text file.
	 * @param strFileName - Name of the file.
	 * @param strData - What we are writing to the file.
	 */
	private function _write(){
		//create our handler
		$handle = fopen($this->_filename, 'a+');
		
		if(is_writable($this->_filename)){
			fwrite($handle, "\r\n" . $this->_encode($this->_data));
		}
		
		fclose($handle);		
	}
	
	/**
	 * @desc Format method - Removes blank lines and decodes all the rest;
	 * @param Array - The conents of the file in an array format.
	 * @echo the decoded lines.
	 */	
	private function _format($array){
		//loop through and decode each line, also remove any white space.
		foreach($array as $line){
			if($line == ""){
				unset($line);
			}else{
				echo '<code>' . $this->_decode($line) . '</code><br>';
			}
		}
	}
	
	/**
	 * @desc Read a file
	 * @param The file we are reading
	 */
	public function read($file){
		$handle = fopen($file, 'r');
		
		//get each line from the file whilst the handle is open and put them into an array
		while(!feof($handle)){
			$data = fgets($handle);
			$data = trim($data);
			
			$array[] = $data;
		}
		
		$this->_format($array);
		
		fclose($handle);
	}
	
	/**
	 * @desc Check method - Make sure that the file chosen to write to is not empty and is a text file.
	 * @param item - The passed destination file.
	 */
	private function _check($item){
		if(!empty($item)){
			if(in_array($this->_get_ext($item), array('txt'))){
				return true;
			}else{
				$error->message(108);
			}
		}else{
			return false;
		}
	}
	
	private function _get_ext($file_name) {
		return end(explode(".", $file_name));
	}
	
	/**
	 * @desc Set the filename value.
	 * @param FileName - Name of the file.
	 */	
	private function _setFileName($FileName){
		$this->_filename = $FileName;
	}
		
	/**
	 * @desc Set the data value.
	 * @param Data of the file.
	 */	
	private function _setData($Data){
		$this->_data = $Data;
	}
	
	/**
	 * @desc Encode data.
	 * @param Text the string I am encoding.
	 */	
	private function _encode($text){ 
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
	} 
	
	/**
	 * @desc Decode data.
	 * @param Text the string I am decoding.
	 */
	private function _decode($text){ 
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
	} 

}