<?php

/**
 *  @desc - User CLASS
 *  @version 1.2.4
 */

class User extends Session{
  
  /* PRIVATE */
  private $_username;
  private $_password;
  private $_db_username;
  private $_db_password;
  private $_db_id;
  private $_sessID;
  private $_role;
  private $_info;
  
  public $db, $db2;
  public $userList = array();
  public $info;
  public $id;
  
  
  function __construct(){
     $this->db = new Database();
  }
  
  /**
   * @desc Login method - Log a user into the system
   * @param username - The users chosen login name.
   * @param password - The password used to login to a user.
   */
  function login($username, $password){
    $this->_username = $username;
    $this->_password = $this->encrypt($password);   
    $rand = rand(0,99999999);
    $this->_sessID = MD5($rand . $this->_username . 'abcdefghijklmnopqrstuvwxyz');
    
   
 
      //var_dump($_POST);
      //testing the inserting part
      //$db->insert($table, $cols, $values);
      //testing the select from database.
      $name = $this->_username;
      $this->db->select('*', 'login_details', "`username` = '$name'");
      
      $data = $this->db->data;
      //var_dump($data);
    
      $this->_db_username = $data[0]['username'];
      $this->_db_password = $data[0]['password'];
      $this->_db_id = $data[0]['id'];
      $this->_role = $data[0]['role'];
      
    //var_dump($this->_password);
    //var_dump($this->_db_password);
      
      if($this->_password == $this->_db_password){
        
        $cols = "sessID";
        $value = "'$this->_sessID'";
        $table = "login_details";
    
    
        //the inserting part
        $id = $this->_db_id;
        $this->db->update($table, $cols, $value, "id = '$id'");
        $this->db->push();
        
        $_SESSION['session_id'] = $value;
        $_SESSION['role'] = strtolower($this->_role);
        $_SESSION['username'] = $this->_db_username;
        $_SESSION['id'] = $id;
        //var_dump($_SESSION);
        
        
        
        $_SESSION['logged_in'] = 'yes';
        $this->_redirect();
        
        
      }else{
        $this->message(105);
      }

  }

  
  /**
   * @desc Check method - Checking to see if the user details are empty, can be used to check and escape strings when a db is involved.
   * @param user - Array containting user details we would like to check.
   */
  public function check($user){
    $num = count($user);
    for($i = 0; $i < $num; $i++){
      if(!empty($user[$i])){
        return true;
      }else{
        return false;
      }
    }
  }
  
  /**
   * @desc Encrypt method - Encrypting strings using sha1 with a predefined salt.
   * @param item - This is the thing we are encrypting.
   */
  public function encrypt($item){
    //This process is not reversible.
    return MD5(sha1($item . 'Iamag33ky3s1am1337')); 
  }
  
  /**
   * @desc Logout method - Logging a user out from the system.
   */
  function logout(){
    //destroy the session and redirect them back to the login page.
    $this->destroy();
    $this->_redirect('login');
  }
  
  /**
   * @desc is_admin - Check to see if the current logged in user is an admin.
   */
  public function is_admin(){
    $id = $_SESSION['id'];
    $this->db->select('*', 'login_details', "`id` = '$id'");
    
    $role = $this->db->data[0]['role'];
    
    if(strtolower($role) != 'admin'){
      $this->_redirect();
    }
  }
  
  /**
   * @desc is_manager - Check to see if the current logged in user is a manager.
   */
  public function is_manager(){
    $id = $_SESSION['id'];
    $this->db->select('*', 'login_details', "`id` = '$id'");
    
    $role = $this->db->data[0]['role'];
    
    if(strtolower($role) != 'manager'){
      $this->_redirect();
    }
  }
  
  /**
   * @desc is_user - Check to see if the current logged in user is an user. This is only for restricting access for user only pages.
   */
  public function is_user(){
    //Check if the current logged in user is of role User.
    $id = $_SESSION['id'];
    //Get the role for the current user from the database.
    $this->db->select('*', 'login_details', "`id` = '$id'");
    
    $role = $this->db->data[0]['role'];
    
    //Check.
     if(strtolower($role) != 'user'){
      $this->_redirect();
    }
  }
  
  
  /**
   * @desc check_username - Checks to see if that user name already exists in the database.
   */
  private function check_username($value){
      //return all values from the database where the username is the same as the requested username.
      $this->db->select('*', 'login_details', "`username` = '$value'");
      //Should only return 1 maximum. Check if its not empty or null and return false, else return true.
      if($this->db->data[0]['username'] != NULL || !empty($this->db->data[0]['username'])){
        return false;
      }else{
        return true;
      }
  }
  
  /**
   * @desc Create - Register a new User.
   * @param - The post array.
   */
  public function create($info){
    //Story the array privately
    $this->_info = $info;
    
    //Clean out every item in the array.
    foreach ($this->_array as $key => $value){
      $this->db->cleanse($value);
    }
    //var_dump($this->_array);
    
    $username = $this->_array['username_create'];
    
    if($this->check_username($username)){
        $password = $this->encrypt($this->_array['password_create']);
        $role = $this->_array['role'];
        
        //store all of the values so it looks tidier.
        $cols = "username, password, role";
        $values = "'$username', '$password', '$role'";
        // and finally insert the details.
        $query = $this->db->insert('login_details', $cols, $values);
        
        //push
        //$this->db->push();
        
        if(!$query){
          $this->message("The query isnt working");
        }
        
    }else{
      $this->message("The username has already been taken.");
    }
  }
  
  /**
   * @desc Get all of the users from the database.
   *  @return - Returns a list of users and their roles.
   */
  public function getUsers(){
  //selet all of the users.
  $query = $this->db->select('*', 'login_details'); 
  $array = $this->db->data;
  
  if(!$query){
    $this->message(113);
  }
  
  for($i = 0; $i < count($array); $i++){
    echo $this->userList[] = '<a href="index.php?page=profile.php&u='. $array[$i]['username'] .'">' . $array[$i]['username'] . ' (' . $array[$i]['role'] . ')</a><br>';
  }
  
  }
  
  /**
   * @desc - get_user_id - Get's all of the details for the user then we take the id.
   * @param - $user - this is the user that we are passing through.
   */
  public function get_user_id($user){
    if(!empty($user)){
    $query = $this->db->select('*', 'login_details', "`username` = '$user'");
      if(!$query){
        $this->message(112);
      }
    return $this->id = $this->db->data[0]['id'];
    }
  }
  
  private function _hasDetails($id){
      $this->db->select('*','staff_details',"`user` = '$id'");
      if(!empty($this->db->data[0])){
          return true;
      }
      
      return false;
  }
  
  public function get_info($user){
    $this->get_user_id($user);
    $id = $this->id;
    if(!empty($user)){
      $query = $this->db->select('*','staff_details',"`user` = '$id'");
      if(!$query){
        $this->message(115);
      }
      echo'<form action="" method="POST">
           <h3>Title:</h3>
           <p><input name="title" type="text" value="'.($query ? $this->db->data[0]['title'] : "N/A").'"></p>
           <h3>Forname:</h3>
           <p><input name="forename" type="text" value="'.($query ? $this->db->data[0]['forename'] : "N/A").'"></p>
           <h3>Surname:</h3>
           <p><input name="surname" type="text" value="'.($query ? $this->db->data[0]['surname'] : "N/A").'"></p>
           <h3>Email:</h3>
           <p><input name="email" type="text" value="'.($query ? $this->db->data[0]['email'] : "N/A").'"></p><br>
           <p><input type="submit" id="button" name="submit"></p></form>';
    }
  }
  
  public function update($array, $user){
    //Story the array privately
    $this->_info = $array;
      
    //Clean out every item in the array.
    foreach ($this->_info as $key => $value){
       $this->db->cleanse($value);
    }
      
    $this->get_user_id($user);
    if(filter_var($array['email'], FILTER_VALIDATE_EMAIL)){
       $email = $this->_info['email'];
       $forename = $this->_info['forename'];
       $surname = $this->_info['surname'];
       $title = $this->_info['title'];
        
       // and finally insert the details.
       if($this->_hasDetails($this->id)){
           //store all of the values so it looks tidier.
            $cols = array('forename', 'surname', 'email', 'title');
            $values = array('"' . $forename . '"','"' . $surname. '"', '"' . $email . '"', '"' . $title . '"');
            $cond = "`user` = " . $this->id;
            $this->db->update('staff_details', $cols, $values, $cond);
       }else{
            $cols = "user, forename, surname, email, title";
            $values = "'$this->id','$forename', '$surname', '$email', '$title'";
            $this->db->insert('staff_details', $cols, $values); 
       }
        
       //push
       //$this->db->push();
       $this->_redirect('panel');
       
       
    }else{
      $this->message("The email you are trying to use is not a valid email.");
    }
     
  }
  
  
}