<?php

/**
 * @desc Ticket class.
 * @version 0.1
 */
 
 class Ticket extends Error{
 	private $_info, $_custID;
     
 	function __construct(){
            $this->database = new Database();
            $this->database2 = new Database();
            $this->user = new User();
 	}
 	
 	public function createCustomer($info){
            //We want everything to be cleaned.
            //Clean out every item in the array.
            foreach ($info as $key => $value){
                $this->database->cleanse($value);
                $info[$key] = $value;
            }
            
            if(filter_var($info['email'], FILTER_VALIDATE_EMAIL)){
                $email = $info['email'];
                $forename = $info['forename'];
                $surname = $info['surname'];
                $title = $info['title'];

                $cols = "forename, surname, email, title";
                $values = "'$forename','$surname', '$email', '$title'";
                $query = $this->database->insert('customer_info', $cols, $values);
                //$this->db->push();
                   
                header("location: Tickets");
                if(!$query){
                    //echo 'Did not insert!';
                }
            }else{
                $this->message("The email provided is not a valid email.");
            }
            
 	}
        
        
        private function _getCustId($fore, $sur){
            $this->database->select('*','customer_info',"`forename` = '$fore' and `surname` = '$sur'");
            return $this->_custID = $this->database->data[0]['cust_id'];
        }
        
        public function createTicket($info, $user){
            //We want everything to be cleaned.
            //Clean out every item in the array.
            foreach ($info as $key => $value){
                $this->database->cleanse($value);
                $info[$key] = $value;
            }
            
            //set up our variables.
            $id = $info['id'];
            $details = $info['details'];
            $status = $info['status'];
            $subject = $info['subject'];
            $priority = $info['$priority'];
            
            //Get the users ID
            $this->user->get_user_id($user);
            $userId = $this->user->id;
            
            //Set up the statement & insert
            $cols = "message, cust_id, status, staffId, subject, priority";
            $values = "'$details','$id', '$status', '$userId', '$subject', '$priority'";
            $this->database->insert('tickets', $cols, $values);
            //spush
            //$this->db->push();
        
 	}
        
        public function viewTicket(){
            
        }
        
        public function viewTickets($user){
            $this->user->get_user_id($user);
            $id = $this->user->id;
            if(!empty($user)){
                
                $query = $this->database->select('*','tickets',"`staffId` = '$id'");
              
                if(!$query){
                    $this->message(115);
                }
                
                for($i = 0; $i < count($this->database->data); $i++){
                    echo'<h3>Fault ID:</h3>
                         <p>'.($query ? $this->database->data[$i]['id'] : "N/A").'</p>
                         <h3>Customer ID:</h3>
                         <p>'.($query ? $this->database->data[$i]['cust_id'] : "N/A").'</p>
                         <h3>Subject:</h3>
                         <p>'.($query ? $this->database->data[$i]['subject'] : "N/A").'</p>
                         <h3>Details:</h3>
                         <p>'.($query ? $this->database->data[$i]['message'] : "N/A").'</p>
                         <h3>Status:</h3>
                         <p>'.($query ? $this->database->data[$i]['status'] : "N/A").'</p>
                         <h3>Priority:</h3>
                         <p>'.($query ? $this->database->data[$i]['priority'] : "N/A").'</p>
                         <br><h2 style="margin-bottom: 40px;"></h2><br>';
                }
            }
        }
        
 	public function update(){
 	
 	}
 	
 	public function delete(){
 	
 	}
 	
 }