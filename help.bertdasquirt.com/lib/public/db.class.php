<?php

/* THIS IS THE DATABASE CLASS:
  It will do:
  Query, get rows, values, delete, update, ?back-up?.
  
*/

class Database{
  
  /**
   * @version 0.9.2
   */
  public $open, $mysqli, $data;
  
  /**
   * @desc - we want to create a connection to the database, but only if we say we need to. 
   */
  public function __construct(){
    //Connect to the database if we start the db class.
    $this->mysqli = new mysqli('localhost', 'root', '', 'alphareport');
    //$mysqli->query("INSERT INTO test (value) VALUES('test')");
    $this->mysqli->autocommit(true);
    
  }
  
  /**
   * @desc - An easy way to close the connection to the database.
   */
  private function _close_connection(){
    // close our database connection.
    $thread_id = $this->mysqli->thread_id;
    $this->mysqli->kill($thread_id);
    $this->mysqli->close();
  }
  
  /**
   * @desc  Insert values into the database.
   * @param Table - this is the table that we want to insert into.
   * @param Values - this is the data that we want to insert.
   * @param Cols -  The columns that we are inserting into.
   */
  public function insert($table, $cols, $values, $condition = false){
    //put the parameters into an array so that we can loop through and check them
    $params = array($table, $cols, $values);
    
    if($condition == false){
      $cond = "";
    }else{
      $cond = " WHERE " . $condition;
    }
    
    //store the statement format
    $statement = "INSERT INTO " . $table . " (" . $cols . ") VALUES(" . $values . ")" . $cond;
    
    //print_r($statement);
    
    //check the params
    if($this->_check($params)){
      //do the insert query
      $query = $this->mysqli->query($statement);
    }

    //provide an error if it doesn't work.
    if(!$query){
      echo 'Insert Query Not Working.';
      var_dump($statement);
    }
    
    $this->_close_connection();
  }
  
  /**
   * @desc  Update rows.
   * @param Table - this is the table that we want to update.
   * @param Values - this is the data that we want to insert.
   * @param Cols -  The columns that we are updating.
   */
  public function update($table, $cols, $values, $condition = false){
    //put the parameters into an array so that we can loop through and check them
        
    if($condition == false){
      $cond = "";
    }else{
      $cond = " WHERE " . $condition;
    }
    for($i = 0; $i < count($cols); $i++){
           $s .= '`' . $cols[$i] .'` = ' . $values[$i] .  ', '; 
    }
    
    $s = substr($s, 0, -2);
    //print_r($s);
    $statement = "UPDATE `" . $table . "` SET " . $s  . " " . $cond;
    //do the update query
    $query = $this->mysqli->query($statement);
    /*"UPDATE `ulog` SET `logged_in`=0, `signout_time`=NOW(),
     `ckey`= '', `ctime`= '' WHERE user_id=? AND logged_in=1"*/
    //provide an error if it doesn't work.
    if(!$query){
      echo 'Update Query Not Working.';
      var_dump($statement);
    }
    $this->_close_connection();

  }
  
  /**
   * @desc - Commit to the database.
   */
  public function push(){
    //$this->mysqli->commit();
    //echo 'you have pushed this!';
  }
  
  /**
   * @desc - Check the data, make sure that it isn't empty, and make sure that it is not harmful
   */
  private function _check($stuff){
    foreach($stuff as $values){
      $this->mysqli->real_escape_string(strip_tags($values));
      if(!empty($values)){
        return true;
      }else{
        return false;
      }
    }
  }
  
  
  /*
  
  public function edit(){
  
  }
  */
  
  
  public function select($what, $where, $condition = false){
    
    $params = array($what, $where);
    
    if(empty($condition) || $condition == ''){
      $cond = "";
    }else{
      $cond = "WHERE " . $condition;
    }
    
    //store the statement format
    $statement = "SELECT " . $what . " FROM " . $where . " " . $cond ."";
    
    //print_r($statement);
    
    //check the params
    //do the SELECT query
    $query = $this->mysqli->query($statement);
    $this->data = array();
    while($row = $query->fetch_assoc()){
        $this->data[] = $row;
    }
    return $this->data;    
    
    //provide an error if it doesn't work.
    if(!$query){
      echo 'Select Query Not Working.';
    }
    //$this->_close_connection();

  }
  
  /**
   *  @desc - Delete data from the database.
   *   @param Where = The table we are deleting from.
   *  @param Cond = the condition on which we are deleting.
   */
  public function delete($what, $where, $cond){
    
    $params = array($what, $where, $what);
    
    $statement = "DELETE" . $what  . " FROM " . $where . " WHERE " . $cond;
    
    if($this->_check($params)){
      $query = $this->mysqli->query($statement);
    }
    
    //provide an error if it doesn't work.
    if(!$query){
      echo 'Delete query not working.';
    }
    $this->_close_connection();

     
  }
  
  /**
   * @desc Cleanse anything that we pass through it.
   * @param dirt - The thing that needs cleaning.
   */
  public function cleanse($dirt){
    //Get rid of anything that we do not need.
    return htmlentities(strip_tags(trim($dirt)));
  }
  
  
}

?>