<?php


/**
 *	UPLOAD Class
 */

class Upload{
	
	private $_filename, $_type, $_file_data, $_dir;
	
	/**
	 *	 @desc Contsructing the class
	 *   @param f This is the name of the file that is being uploaded.
	 *   @param t This is the type of file that is being uploaded.
	 *   @param d This is the data for the file.
	 *   @param directory This is the directory which we are saving the file in.
	 */
	public function __construct($f, $t, $d, $directory){
		$this->_filename = $f;
		$this->_dir = $directory;
		$this->_file_data = $this->_check($d);
		
		if(in_array($t, array('.png', '.jpg', '.jpeg', '.gif', '.bmp'))){
			$this->_type = $t;
		}else{
			return $error->message(102);
		}
		
		
	}
	
	private function _check($data){
		return (!empty($data) && is_writable($this->_dir));
	}
		
}