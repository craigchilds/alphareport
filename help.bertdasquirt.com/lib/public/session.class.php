<?php

class Session extends Error{
	
	protected $db;
	protected $test = "hello world";
	private $_new_sessID;
	/**
	 *  @desc Contruct our class, Start our session if the variable is empty.
	 */
	public function __construct(){
		// start the session.
		session_start();
		$this->db = new Database();
	}
	
	/**
	 *  @desc Set our session variables.
	 *  @param What - This is what we are setting.
	 *  @param Value - This is what we are setting it to.
	 */
	protected function set($what, $value){
		$_SESSION[$what] = $value;
	}
	
	/**
	 *  @desc Get our session variables.
	 *  @param Key - This is the key of the session array which we are Getting.
	 */
	protected function get($key){
		return (!empty($_SESSION[$key]));
	}
	
	/**
	 *  @desc Var dump our session.
	 *  @param Key - This is what we are DISPLAYING.
	 */
	protected function display($key){
		var_dump($_SESSION[$key]);
	}
	
	/**
	 * @desc Update the id in the database, destroy the session and unset the variables.
	 */
	protected function destroy(){
		
		//we want to change the id in the database. SO that the user cannot have a session unless they login.
		$rand = rand(0,99999999);
		$this->_new_sessID = MD5($rand . $_SESSION['username'] . 'abcdefghijklmnopqrstuvwxyz');
		
		$cols = "sessID";
		$value = "'$this->_new_sessID'";
		$table = "login_details";
		
		
		//the inserting part
		$id = $_SESSION['id'];
		$this->db->update($table, $cols, $value, "id = '$id'");
		$this->db->push();
		
		//destroy all of our session related content.
		unset($_SESSION);
		session_unset();
		session_destroy();
	}
	
	/**
	 * @desc Check to see if someone is already logged in with a session.
	 */
	public function check($value = false){
		if($_SESSION['logged_in'] == 'yes'){
		   $id_of_user = $_SESSION['id'];
		   $this->db->select('*', 'login_details', "`id` = $id_of_user");
		   $db_session_id = '\'' . $this->db->data[0]['sessID'] . '\'';
		   
		   if($_SESSION['session_id'] == $db_session_id){
		     if($value){$this->_redirect();}
		     return true;
		   }
		  
		}else{
			return false;
		}
	}
	
	/**
	 * @desc Redirect a user to the last page they were on, the index, or a requested page.
	 * @param to, this is where we want a user to be redirected to.
	 */
	protected function _redirect($to = false){
	  if(strpos($_SERVER['HTTP_REFERER'], "http://help.bertdasquirt.com")){
	  	header("location: " . $_SERVER['HTTP_REFERER']);
	  }else if(empty($to)){
	  	header("location: index");
	  }else{
	  	header("location: " . $to);
	  }
	}
}	