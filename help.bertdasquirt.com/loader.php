<?php


// create a constant root to make things look tidier
//define(ROOT, $path);
//define(DB_ROOT, "/lib/private/db.class.php");

class GetLib{
  
  public static $page, $controller, $subpage, $db, $user, $path;
    
  /**
   * @desc Load all of our library classes.
   */
  function __construct(){
    error_reporting(E_ALL ^ E_NOTICE);
    //loop through and include every class. 
    foreach(glob("lib/public/*.class.php", GLOB_ERR) as $dir){
      require_once($dir);    
    }
        
  }
 
  
  /**
   * @desc Find out what page is being requested, if exists include it, else throw 404
   */
  function getPage() {
    
    $this->path = str_ireplace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
    $this->page = str_replace('.php', '', stripslashes(htmlentities($_GET['page'])));
    
    if(substr($this->page, -1) == '/'){
      $this->page = $this->page . 'index';
    }
    
    
    if($this->_check($this->page)){
      include_once($this->path . '/html/' . $this->page . '.php');
    }else if($this->page == ''){
      include_once($this->path . '/html/index.php');
    }else{
      include_once($this->path . '/html/404.php');
    }
    
    //print_r('<br>' . $this->page);
  }
    
  
  /**
   * @desc Check our page request against our array of allowed pages. 
   * @param $val the page that we are checking.
   * @return true or false outcome.
   */
  private function _check($val){
    return (!empty($val) && file_exists($this->path . '/html/' . $val . '.php'));
  }
  
  /**
   *   @desc Get the controller for the page if our array says we need to.
   */
  function getController(){
    //This list tells us what pages haven't controllers
    $controller_list = array('404' => true, 'log' => true);
    
    //set the public property to the value of the array in accordance to the current page.
    $this->controller = $controller_list[$this->page];
    
    //If true include it. 
    if(!$this->controller){
        require_once($this->path . '/controllers/' . $this->page . '.php' );
    }
  }
  
  
  
}