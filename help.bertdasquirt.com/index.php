<?php
 ob_start();

/* THIS PAGE IS THE MAIN CONTROLLER FOR EVERY VIEW */

//get all of our libraries using the GetLib class which will get them for us.
require_once("loader.php");
$getlib = new GetLib();

//We want to find out what page is being requested, don't we?
$getlib->getPage();

//we want to get our controller don't we?
$getlib->getController();

//array of stuff that is being written to the text file.
$array = array($_SERVER['REMOTE_ADDR'], time(), $_SERVER['PHP_SELF'], $_GET['page'], $_SESSION['username']);

/**
 * This is how you read, write and read+write the file.
 * State = "r" - READ, "w" - WRITE, "r+w" - READ AND WRITE
 * Array is the stuff you want to put in, MUST BE AN ARRAY
 * If you want to just read - '' in first param, filename in second and third is state = "r"
 * example use : $obj = new Log(array, filename, state = what you want to do)
 */
if($_GET['page'] != "log"){
    $log = new Log($array, 'filelog.txt', 'w');
}
  
?>