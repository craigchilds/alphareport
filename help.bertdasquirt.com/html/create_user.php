<?php 
session_start();
 ?>
<!doctype html>
<!-- COMMENTS FTW! -->
<html>
	<?php include 'includes/header.php'; ?>
	
	<body>
		<div id="container">
			<div id="top-bar">
				<p id="top-text"> <strong> Current User:</strong> <a href="#" id="username"><?php if(!empty($_SESSION['username'])){ echo $_SESSION['username'] . '</a> | <a id="logout" href="logout">Logout</a>';}else{echo 'Guest' . '</a> | <a id="logout" href="login">Login</a>';}?></p>
			</div>
			
			<div id="nav-bar">
				<img id="logo" src="img/logo.png" onclick="goHome()"/>
				<ul>
					<li><img src="img/user.png"/><a id="link" href="profile"> Profile </a></li>
					<li><img src="img/chart_bar.png"/><a id="link" href="panel"> Panel </a></li>
					<li><img src="img/page.png"/><a id="link" href="tickets"> Tickets </a></li>
					<li><img src="img/house.png"/><a id="link" href="index"> Home </a></li>
				</ul>
			</div>
			
			<div id="content">
				<h2>Create New User</h2>

				<br>
				<form name="create_user" action="" method="POST">
					<br>
					<h4>Username</h4>
					<p>
						<input type="text" class="textbox" placeholder="Type the users fornename here" name="username_create">
					</p>
					<br>
					<h4>Password</h4>
					<p>
						<input type="password" class="textbox" placeholder="Type the users surname here" name="password_create">
					</p>
					<br>
					<h4>Role</h4>
					<p>
						<select id="select" placeholder="Type the users role here" name="role">
							<option> User </option>
							<option> Manager </option>
							<option> Admin </option>
						</select>
					</p>
					<br><!--
					<h4>Upload Image</h4>
					<p>
						<input type="file" class="textbox" name="image">
					</p>
					<br>-->
					<br>
					
					<p><input type="submit" id="button" style="float: left;" value="Submit" name="submit"></p>
				</form>
				
				<div class="right">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
					<br>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
				</div>
				<br>
			</div>
		</div>
	</body>
</html>