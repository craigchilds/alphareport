<?php 
session_start();
 ?>
<!doctype html>
<!-- COMMENTS FTW! -->
<html>
	<?php include 'includes/header.php'; ?>
	
	<body>
		<div id="container">
			<div id="top-bar">
				<p id="top-text"> <strong> Current User:</strong> <a href="#" id="username"><?php if(!empty($_SESSION['username'])){ echo $_SESSION['username'] . '</a> | <a id="logout" href="logout">Logout</a>';}else{echo 'Guest' . '</a> | <a id="logout" href="login">Login</a>';}?></p>
			</div>
						
			<div id="nav-bar">
				<img id="logo" src="img/logo.png" onclick="goHome()"/>
				<ul>
					<li><img src="img/user.png"/><a id="link" href="profile"> Profile </a></li>
					<li><img src="img/chart_bar.png"/><a id="link" href="panel"> Panel </a></li>
					<li><img src="img/page.png"/><a id="link" href="tickets"> Tickets </a></li>
					<li><img src="img/house.png"/><a id="link" href="index"> Home </a></li>
				</ul>
			</div>
			
			<div id="content">
				<h2>My Tickets</h2>
				</br>
				<p style="float:left;">	
					<?php 
						$ticket = new Ticket();
						$ticket->viewTickets($_SESSION['username']);
					?>
				</p>
							
			</div>
		</div>
	</body>
</html>