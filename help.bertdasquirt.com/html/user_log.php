<?php 
session_start();
 ?>
<!doctype html>

<!-- COMMENTS FTW! -->
<html>
	<?php include 'includes/header.php'; ?>
	
	<body>
		<div id="container">
			<div id="top-bar">
				<p id="top-text"> <strong> Current User:</strong> <a href="#" id="username"><?php if(!empty($_SESSION['username'])){ echo $_SESSION['username'] . '</a> | <a id="logout" href="logout">Logout</a>';}else{echo 'Guest' . '</a> | <a id="logout" href="login">Login</a>';}?></p>
			</div>
			
			<div id="nav-bar">
				<img id="logo" src="img/logo.png" onclick="goHome()"/>
				<ul>
					<li><img src="img/user.png"/><a id="link" href="profile"> Profile </a></li>
					<li><img src="img/chart_bar.png"/><a id="link" href="panel"> Panel </a></li>
					<li><img src="img/page.png"/><a id="link" href="tickets"> Tickets </a></li>
					<li><img src="img/house.png"/><a id="link" href="index"> Home </a></li>
				</ul>
			</div>
			
			<div id="content">
				<h2>User Log</h2>
				<?php $log = new Log('', '/log.txt','r'); ?>
				</br>
				<div class="right">
					<p>Here is the log, it provides you with a list of recent users, tracking their usage of the system. Using this you can track staff members and prevent potential threats.</p>
					<br>
				</div>
				<br>				
				
			</div>
			<!--<div id="footer">
				<p>
					Copyright &copy; Craig Childs, Alpha Report 2011-2012.
				</p>
				<p id="note">
					Please note this website is part of a college project and is not available for public use. 
				</p>
			</div>-->
			
			
		</div>
	</body>
</html>