
<!doctype html>
<html>
<head>
	<link rel="stylesheet" href="/css/style.css" type="text/css">
    <title> <?php echo 'Alpha Report - ' . $_GET['page']; ?> </title>
	<link rel="shortcut icon" href="/img/favicon.ico">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
	<div class="container">
		<div id="top-bar">
			<p id="top-text"> <strong> Current User:</strong> <a href="#" id="username">Admin</a> - Last logged in 10/11/2011 at 12:37pm | <a id="logout" href="#">Logout</a></p>
			
		</div>
		<div id="header">
			<img id="logo" src="/img/logo.png" onclick="goHome()"/>
			<ul>
				<li><a id="link" href="index"> Tickets </a></li>
				<li><a id="link" href="index"> Logs </a></li>
				<li><a id="link" href="index"> Statistics </a></li>
				<li><a id="link" href="index"> Profile </a></li>
				<li id="selected"><a id="link" href="index"> Users </a></li>
				<li><a id="link" href="index"> Home </a></li>
			</ul>
		</div>			<div id="content">
				<div id="menu">
				<ul>
					<li id="selected"><a href="index"> Create </a></li>
					<li><a href="index"> Edit </a></li>
					<li><a href="index"> View </a></li>
					<li><a href="index"> Stats </a></li>
					<li><a href="index"> Delete </a></li>
				</ul>
				</div>
				<div id="page">
					<h1> Create a new user </h1>
					<p>This section will allow you to create a new user for the system, only admins can create new users. Below you will be required to input all of the users login details.</p>
					<br>
					<p>
						<form name="test" action="#">
							<em>Username: </em>
							<p>
								<input type="textbox" class="textbox" placeholder="Type a username here">
							</p>
							<em>Password: </em>
							<p>
								<input type="password" class="textbox" placeholder="Type a password here">
							</p>
							<em>Repeat Password:</em>
							<p>
								<input type="password" class="textbox" placeholder="Repeat the password">
							</p>
							<em>Role:</em>
							<p>
								<select id="select"> 
									<option value="user">User</option>
									<option value="manager">Manager</option>
									<option value="admin">Admin</option>									
								</select>
							</p>
							<em>Bio:</em>
							<p><textarea id="textarea" placeholder="Testing 123"></textarea></p>
							<p><input type="submit" id="button" value="Submit"></p>
						</form>
					</p>
				</div>
			</div>
			
			<div id="footer">
				<p> Craig Childs &copy; Alpha Reporter 2011. </p>
				<p>
					<a href="about.html">About Us</a>
					|
					<a href="help.html">Help</a>
					|
					<a href="contact.html">Contact Us</a>
				</p>
			</div>
		</div>
</body>
</html>