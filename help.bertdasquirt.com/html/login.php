<?php 
session_start();
 ?>
<!doctype html>


<!-- COMMENTS FTW! -->
<html>
  <?php include 'includes/header.php'; ?>
  
  <body>
    <div id="container">
      <div id="top-bar">
        <p id="top-text"> <strong> Current User:</strong> <a href="#" id="username"><?php if(!empty($_SESSION['username'])){ echo $_SESSION['username'] . '</a> | <a id="logout" href="logout">Logout</a>';}else{echo 'Guest' . '</a> | <a id="logout" href="login">Login</a>';}?></p>
      </div>
      
      
      <div id="nav-bar">
        <img id="logo" src="img/logo.png" onclick="goHome()"/>
        <ul>
          
        </ul>
      </div>
      
      <div id="content">
        <h2>Login</h2>
        </br>
        <form name="login" action="" method="POST">
          <h4>Username</h4>
          <p>
            <input type="text" class="textbox" placeholder="Type your username here" name="username">
          </p>
          <br>
          <h4>Password</h4>
          <p>
            <input type="password" class="textbox" placeholder="Type your password here" name="password">
          </p>
          <br>
          <p><input type="submit" id="button" style="float: left;" value="Submit" name="submit"></p>
          <br>
          <p><a href="forgot" id="forgot">Forgot password?</a>  <a href="request_account" id="forgot">Need an account?</a></p>
        </form>
        
        <div class="right">
          <p>Please <strong>login</strong> using the credentials that have been provided for you. You cannot access any other area of the site unless you are logged in.</p>
          <br>
          <p>If you have <strong>forgotten </strong>your password we will reset it for you. To do so please click on forgot password below the login form. The username and password fields are case sensitive so make sure that<strong> caps lock </strong> is off</p>
        </div>
        
        
      </div>
    </div>
  </body>
</html>